import distro
import pytest

from leapp.models import LeftoverPackages


@pytest.mark.skipif(
    distro.linux_distribution()[0]
    not in ('Fedora', 'CentOS Linux', 'Red Hat Enterprise Linux Server'),
    reason='Should run on RPM based distros.',
)
def test_actor_execution(current_actor_context):
    current_actor_context.run()
    assert current_actor_context.consume(LeftoverPackages)
